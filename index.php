<?php

require_once __DIR__ . '/vendor/autoload.php';

use App\Matrix;


$rows = mt_rand(2, 5);
$cols = mt_rand(2, 5);


$matrix = new Matrix($rows, $cols);


echo "Matriz generada con números impares:" . PHP_EOL;
$matrix->printMatrix();
