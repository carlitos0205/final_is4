<?php

namespace App;

class Matrix
{
    private $matrix;

    public function __construct($rows, $cols)
    {
        $this->generateMatrix($rows, $cols);
        $this->fillWithOddNumbers();
    }

    private function generateMatrix($rows, $cols)
    {
        $this->matrix = [];
        for ($i = 0; $i < $rows; $i++) {
            for ($j = 0; $j < $cols; $j++) {
                $this->matrix[$i][$j] = 0; 
            }
        }
    }

    private function fillWithOddNumbers()
    {
        foreach ($this->matrix as $i => &$row) {
            foreach ($row as $j => &$cell) {
                $cell = mt_rand(1, 100); 
                if ($cell % 2 == 0) {
                    $cell++; 
                }
            }
        }
    }

    public function printMatrix()
    {
        foreach ($this->matrix as $row) {
            echo implode(' ', $row) . PHP_EOL;
        }
    }
}
